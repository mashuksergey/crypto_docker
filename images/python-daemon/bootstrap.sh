#!/bin/sh

if [ "$JUST_RUN" = "N" ]; then
  git clone $URL /app
fi

cd /app

if [ "$JUST_RUN" = "N" ]; then
  if [ "$INSTALL" = "Y" ]; then
    pip install -r requirements.txt
  fi
fi

python3 main.py $APP_ARGS
